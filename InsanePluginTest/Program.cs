﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using InsanePluginLibrary.Convert;
using System;
using UnityEngine;

namespace InsanePluginTest
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			ColorTest colorTest = new ColorTest();
			colorTest.TestColorTransformation();

			TemperatureTest temperatureTest = new TemperatureTest();
			temperatureTest.TestTemperatureConversion();

			DistanceTest distanceTest = new DistanceTest();
			distanceTest.TestDistanceConversion();

			Console.ReadLine();
		}
	}

	internal class DistanceTest
	{
		internal void TestDistanceConversion()
		{
			Console.WriteLine("\n-- Distance Conversion Begin");
			LogTest(0.0f);
			LogTest(10.0f);
			LogTest(25.0f);
			LogTest(25.8f);
			LogTest(37.0f);
			LogTest(100.0f);
			Console.WriteLine("\n-- Distance Conversion End\n\n");
		}

		internal void LogTest(float value)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", value));

			Foot foot = value;
			Console.WriteLine(string.Format("Foot: {0}", foot));

			Inch inch = ConvertDistance.ToInch(foot);
			Console.WriteLine(string.Format("Inch: {0}", inch));

			Meter meter = ConvertDistance.ToMeter(inch);
			Console.WriteLine(string.Format("Meter: {0}", meter));

			Mile mile = ConvertDistance.ToMile(meter);
			Console.WriteLine(string.Format("Mile: {0}", mile));

			Yard yard = ConvertDistance.ToYard(mile);
			Console.WriteLine(string.Format("Yard: {0}", yard));

			foot = ConvertDistance.ToFoot(yard);
			Console.WriteLine(string.Format("Foot: {0}", foot));
		}
	}

	internal class TemperatureTest
	{
		internal void TestTemperatureConversion()
		{
			Console.WriteLine("\n-- Temperature Conversion Begin");
			LogTest(0.0f);
			LogTest(10.0f);
			LogTest(25.0f);
			LogTest(25.8f);
			LogTest(37.0f);
			LogTest(100.0f);
			Console.WriteLine("\n-- Temperature Conversion End\n\n");
		}

		internal void LogTest(float value)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", value));
			Celsius celsius = value;
			Console.WriteLine(string.Format("Celsius: {0}", celsius));
			Kelvin kelvin = ConvertTemperature.ToKelvin(celsius);
			Console.WriteLine(string.Format("Kelvin: {0}", kelvin));
			Fahrenheit fahrenheit = ConvertTemperature.ToFahrenheit(kelvin);
			Console.WriteLine(string.Format("Fahrenheit: {0}", fahrenheit));
			celsius = ConvertTemperature.ToCelsius(fahrenheit);
			Console.WriteLine(string.Format("Celsius: {0}", celsius));
		}
	}

	internal class ColorTest
	{
		internal void TestColorTransformation()
		{
			Console.WriteLine("\n-- Color Conversion Begin\n");
			LogTest(Color.black);
			LogTest(Color.white);
			LogTest(Color.red);
			LogTest(Color.green);
			LogTest(Color.blue);
			LogTest(new Color(0.5f, 0.5f, 0.5f, 1f));
			LogTest(new Color(0.2f, 0.4f, 0.6f, 0.8f));
			Console.WriteLine("\n-- Color Conversion End\n\n");
		}

		internal void LogTest(Color color)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", color.ToString()));
			var colorHex = ConvertColor.ToHEX(color);
			Console.WriteLine(string.Format("HEX = {0}", colorHex));

			var hslColor = ConvertColor.ToHSL(colorHex);
			Console.WriteLine(string.Format("HSL = {0}", hslColor.ToString()));

			var hexColor = ConvertColor.ToColor(hslColor);
			Console.WriteLine(string.Format("RGB = {0}", hexColor.ToString()));
		}
	}
}