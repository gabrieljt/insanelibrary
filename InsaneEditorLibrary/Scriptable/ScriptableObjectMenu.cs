﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEditor;
using UnityEngine;

namespace InsaneEditorLibrary.Scriptable
{
	internal class ScriptableObjectMenu
	{
		// Adapted from https://gist.github.com/mstevenson/4726563
		[MenuItem("Assets/Create/Create ScriptableObject From Script", false, 10000)]
		public static void CreateAsset()
		{
			ScriptableObject asset = ScriptableObject.CreateInstance(Selection.activeObject.name);
			AssetDatabase.CreateAsset(asset, string.Format("Assets/{0}.asset", Selection.activeObject.name));
			EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;
		}
	}
}