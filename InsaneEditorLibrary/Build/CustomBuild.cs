﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;
using UnityEditor;
using UnityEngine;

namespace InsaneEditorLibrary.Build
{
	/// <summary>
	/// This class helps to build a project without the need to rewrite the build process for each target platform.
	/// </summary>
	public static class CustomBuild
	{
		/// <summary>
		/// Properly runs the build process.
		/// </summary>
		/// <param name="levels">The level names to be added to the build.</param>
		/// <param name="target">The target platform</param>
		/// <param name="applicationName">The Executable's name (window title)</param>
		/// <param name="options">Build options</param>
		static public void FlexibleBuild(string[] levels, BuildTarget target, string applicationName, BuildOptions options = BuildOptions.None)
		{
			string extension = string.Empty;
			switch (target)
			{
				case BuildTarget.WebPlayer:
				case BuildTarget.WebPlayerStreamed:
					extension = "";
					break;

				case BuildTarget.StandaloneOSXIntel:
				case BuildTarget.StandaloneOSXIntel64:
				case BuildTarget.StandaloneOSXUniversal:
					extension = "app";
					break;

				case BuildTarget.StandaloneWindows:
				case BuildTarget.StandaloneWindows64:
					extension = "exe";
					break;

				default:
					throw new Exception("Platform export not supported");
			}

			string key = string.Format("{0}Last{1}BuildPath", PlayerSettings.productName, target.ToString());

			string basePath = EditorPrefs.HasKey(key) ? EditorPrefs.GetString(key) : Application.dataPath + "/Build";
			if (!System.IO.Directory.Exists(basePath))
			{
				basePath = EditorUtility.SaveFilePanel("Choose location to build", Application.dataPath + "/Build", applicationName, extension);
			}

			if (!string.IsNullOrEmpty(basePath))
			{
				BuildPipeline.BuildPlayer(levels, basePath, target, options);
			}

			EditorPrefs.SetString(key, basePath);
		}
	}
}