﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace InsaneEditorLibrary.Utils
{
	/// <summary>
	/// Editor class for Removing folders
	/// </summary>
	public class FolderChecking : Editor
	{
		[MenuItem("Insane/Utils/Remove Empty Folders")]
		private static void RemoveEmptyFolders()
		{
			RecursiveCheckDirectory(Application.dataPath.Replace('/', '\\'));
		}

		/// <summary>
		/// Checks recursively for empty folders
		/// </summary>
		/// <param name="path">The folder to have its contents checked</param>
		static public void RecursiveCheckDirectory(string path)
		{
			string[] directories = Directory.GetDirectories(path);

			if (directories.Length > 0)
			{
				foreach (string dir in directories)
				{
					RecursiveCheckDirectory(dir);
				}
			}

			string[] _files = Directory.GetFiles(path);
			string[] _directories = Directory.GetDirectories(path);

			if (_files.Length == 0 && _directories.Length == 0)
			{
				string shortpath = path.Substring(Application.dataPath.Length);
				if (EditorUtility.DisplayDialog("Remove Folder", "Delete empty folder \"" + shortpath + "\"?", "Ok", "Cancel"))
				{
					Directory.Delete(path);
					File.Delete(string.Format("{0}.meta", path));
					Debug.LogFormat("Deleted folder {0}\n", shortpath);
				}
				else
				{
					Debug.LogFormat("Skipped folder {0}\n", shortpath);
				}
			}
		}
	}
}