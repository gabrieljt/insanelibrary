﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

namespace InsanePluginLibrary.Threading
{
	/// <summary>
	/// This enumerator contains the debugging masks
	/// </summary>
	[System.Flags]
	public enum DebugLevel
	{
		/// <summary>
		/// No information is debugged
		/// </summary>
		NONE = 1,

		/// <summary>
		/// Only information is debuuged
		/// </summary>
		INFO = 1 << 1,

		/// <summary>
		/// Only Warning information is debuuged
		/// </summary>
		WARNING = 1 << 2,

		/// <summary>
		/// Only errors are debugged
		/// </summary>
		ERROR = 1 << 3,

		/// <summary>
		/// All information is debugged
		/// </summary>
		FULL = INFO | WARNING | ERROR,
	}

	/// <summary>
	/// This class contains the Extensions related to DebugLevel information
	/// </summary>
	public static class DebugLevelExtensions
	{
		/// <summary>
		/// Returns true if the value is contained within this DebugLevel
		/// </summary>
		/// <param name="val1">The current DebugLevel</param>
		/// <param name="val2">The value to be compared</param>
		/// <returns>Returns true if the value is contained within this DebugLevel</returns>
		static public bool Contains(this DebugLevel val1, DebugLevel val2)
		{
			return (val1 & val2) > 0;
		}
	}
}