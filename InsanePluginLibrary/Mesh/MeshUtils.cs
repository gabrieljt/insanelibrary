﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEngine;

namespace InsanePluginLibrary.Mesh
{
	/// <summary>
	/// This class provides a simplistic view of a axis aligned bounding box
	/// </summary>
	public class AABB
	{
		/// <summary>
		/// Most Negative Values
		/// </summary>
		public Vector3 Min;

		/// <summary>
		/// Most Positive Values
		/// </summary>
		public Vector3 Max;

		/// <summary>
		/// The difference between maximum and minimum values
		/// </summary>
		public Vector3 Size
		{
			get
			{
				return Max - Min;
			}
		}

		/// <summary>
		/// Creates a new instance of AABB
		/// </summary>
		public AABB()
		{
			Min = new Vector3();
			Max = new Vector3();
		}
	}

	/// <summary>
	/// This class has some utilities for mesh handling.
	/// </summary>
	public static class MeshUtils
	{
		/// <summary>
		/// Calculates the world-space size of a mesh group
		/// </summary>
		/// <param name="targetObject"></param>
		/// <returns></returns>
		public static AABB GetObjectSize(GameObject targetObject)
		{
			AABB boundingBox = new AABB();

			//Pooling;
			UnityEngine.Mesh currentMesh;
			Vector3 vtxPt;

			//Iterations;
			MeshFilter[] filters = targetObject.GetComponentsInChildren<MeshFilter>();
			foreach (MeshFilter filter in filters)
			{
				currentMesh = filter.mesh;
				int count = currentMesh.vertexCount;
				for (int i = 0; i < count; i++)
				{
					vtxPt = filter.transform.InverseTransformPoint(currentMesh.vertices[i]);
					boundingBox.Min.x = Mathf.Min(boundingBox.Min.x, vtxPt.x);
					boundingBox.Min.y = Mathf.Min(boundingBox.Min.y, vtxPt.y);
					boundingBox.Min.z = Mathf.Min(boundingBox.Min.z, vtxPt.z);

					boundingBox.Max.x = Mathf.Max(boundingBox.Max.x, vtxPt.x);
					boundingBox.Max.y = Mathf.Max(boundingBox.Max.y, vtxPt.y);
					boundingBox.Max.z = Mathf.Max(boundingBox.Max.z, vtxPt.z);
				}
			}

			return boundingBox;
		}
	}
}