﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

namespace InsanePluginLibrary.Convert
{
	/// <summary>
	/// Main class for the temperature conversion
	/// </summary>
	public class ConvertTemperature
	{
		/// <summary>
		/// Converts Celcius temperature value to Fahrenheit
		/// </summary>
		/// <param name="celsius">The Celcius temperature value</param>
		/// <returns>The Fahrenheit temperature value</returns>
		static public Fahrenheit ToFahrenheit(Celsius celsius)
		{
			return (celsius * 9f / 5f) + 32f;
		}

		/// <summary>
		/// Converts Celcius temperature value to Kelvin
		/// </summary>
		/// <param name="celsius">The Celcius temperature value</param>
		/// <returns>The Kelvin temperature value</returns>
		static public Kelvin ToKelvin(Celsius celsius)
		{
			return celsius + 273.15f;
		}

		/// <summary>
		/// Converts Fahrenheit temperature value to Celsius
		/// </summary>
		/// <param name="fahrenheit">The Fahrenheit temperature value</param>
		/// <returns>The Celsius temperature value</returns>
		static public Celsius ToCelsius(Fahrenheit fahrenheit)
		{
			return (fahrenheit - 32f) * 5f / 9f;
		}

		/// <summary>
		/// Converts Fahrenheit temperature value to Kelvin
		/// </summary>
		/// <param name="fahrenheit">The Fahrenheit temperature value</param>
		/// <returns>The Kelvin temperature value</returns>
		static public Kelvin ToKelvin(Fahrenheit fahrenheit)
		{
			return (fahrenheit - 32f) * 5f / 9f + 273.15f;
		}

		/// <summary>
		/// Converts Kelvin temperature value to Celsius
		/// </summary>
		/// <param name="kelvin">The Kelvin temperature value</param>
		/// <returns>The Celsius temperature value</returns>
		static public Celsius ToCelsius(Kelvin kelvin)
		{
			return kelvin - 273.15f;
		}

		/// <summary>
		/// Converts Kelvin temperature value to Fahrenheit
		/// </summary>
		/// <param name="kelvin">The Kelvin temperature value</param>
		/// <returns>The Fahrenheit temperature value</returns>
		static public Fahrenheit ToFahrenheit(Kelvin kelvin)
		{
			return (kelvin - 273.15f) * 9f / 5f + 32f;
		}
	}

	#region Value Types

	/// <summary>
	/// This Struct contains the value and conversion methods for the Fahrenheit temperature unit
	/// </summary>
	public struct Fahrenheit
	{
		private float value;

		private Fahrenheit(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Fahrenheit unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Fahrenheit temperature</param>
		public static implicit operator Fahrenheit(float value)
		{
			return new Fahrenheit(value);
		}

		/// <summary>
		/// Returns the float value of a Fahrenheit temperature
		/// </summary>
		/// <param name="fahrenheit">The Fahrenheit temperature object</param>
		public static implicit operator float(Fahrenheit fahrenheit)
		{
			return fahrenheit.value;
		}

		/// <summary>
		/// Returns a string representation of the Fahrenheit temperature value
		/// </summary>
		/// <returns>A string representation of the Fahrenheit temperature value</returns>
		public override string ToString()
		{
			return string.Format("Fahrenheit ({0})", value.ToString());
		}
	}

	/// <summary>
	/// This Struct contains the value and conversion methods for the Celsius temperature unit
	/// </summary>
	public struct Celsius
	{
		private float value;

		private Celsius(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Celsius unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Celsius temperature</param>
		public static implicit operator Celsius(float value)
		{
			return new Celsius(value);
		}

		/// <summary>
		/// Returns the float value of a Celsius temperature
		/// </summary>
		/// <param name="celsius">The Celsius temperature object</param>
		public static implicit operator float(Celsius celsius)
		{
			return celsius.value;
		}

		/// <summary>
		/// Returns a string representation of the Celsius temperature value
		/// </summary>
		/// <returns>A string representation of the Celsius temperature value</returns>
		public override string ToString()
		{
			return string.Format("Celsius ({0})", value.ToString());
		}
	}

	/// <summary>
	/// This Struct contains the value and conversion methods for the Kelvin temperature unit
	/// </summary>
	public struct Kelvin
	{
		private float value;

		private Kelvin(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Kelvin unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Kelvin temperature</param>
		public static implicit operator Kelvin(float value)
		{
			return new Kelvin(value);
		}

		/// <summary>
		/// Returns the float value of a Kelvin temperature
		/// </summary>
		/// <param name="kelvin">The Kelvin temperature object</param>
		public static implicit operator float(Kelvin kelvin)
		{
			return kelvin.value;
		}

		/// <summary>
		/// Returns a string representation of the Kelvin temperature value
		/// </summary>
		/// <returns>A string representation of the Kelvin temperature value</returns>
		public override string ToString()
		{
			return string.Format("Kelvin ({0})", value.ToString());
		}
	}

	#endregion Value Types
}